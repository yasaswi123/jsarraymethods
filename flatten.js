const nestedArray = [1, [2], [[3]], [[[4]]]]; 
let array=[]
function flatten(nestedArray){
    if(nestedArray.length>0){
        for(let index=0;index<nestedArray.length;index++){
            if(Array.isArray(nestedArray[index])){
                flatten(nestedArray[index])
            }
            else{
                array.push(nestedArray[index])
            }
        }
        return array
    }
    else{
        return "Array is empty"
    }
}

module.exports = {nestedArray,flatten}


