function filter(elements,cb){
    if(elements.length>0){
        filterArray=[]
        for(let index=0;index<elements.length;index++){
            const filterElements=cb(elements[index])
            if(filterElements){
                filterArray.push(elements[index])
            }
        }
        return filterArray
    }
    else{
        return "Array is empty"
    }
}

module.exports=filter