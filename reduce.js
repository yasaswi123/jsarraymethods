function reduce(elements,cb,startingValue){
    if(elements.length>0){
        for(let index=1; index < elements.length;index++){
            startingValue=cb(elements[index],startingValue)
        }
        return startingValue;
    }
    else{
        return "Array is empty"
    }
}

module.exports=reduce

