function find(elements,cb){
    if(elements.length>0){
        for(let index=0 ; index<elements.length;index++){
            if(cb(elements[index])){
                return elements[index]
            }
        }
    }
    else{
        return "Array is empty"
    }
}

module.exports = find