
function map(elements,cb){
    if(elements.length>0){
        const mapArray=[]
        for(let index=0;index< elements.length;index++){
            mapArray.push(cb(elements[index],index))
        }
        return mapArray
    }
    else{
        return "Array is empty"
    }
}

module.exports = map